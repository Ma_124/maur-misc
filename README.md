# Miscellaneous PKGBUILDs
## `awesome-noauto`
Adds an xsession `.desktop` file that launches awesomewm after creating a file at `/tmp/awesome-started`.
My [personal dotfiles][awesome-noauto] will stop executing autostart applications.

## `kitty-xsession`
Launch kitty terminal as xsession.

## `rust-man`
Install rust man pages, bash and zsh completions for the Rust programming languages tooling.
And 'provide' the `cargo` package.

## `chromium-man`
Provide man pages for chromium.

[awesome-noauto]: https://gitlab.com/Ma_124/dotfiles/-/blob/01e0d356833adf569cb63ec5039cd8a1eac31da4/.config/awesome/rules.lua#L105
