.PHONY: clean
clean:
	@rm -rf *.tar* src pkg

.PHONY: awesome-noauto
awesome-noauto:
	@makepkg -p awesome-noauto.PKGBUILD

.PHONY: kitty-xsession
kitty-xsession:
	@makepkg -p kitty-xsession.PKGBUILD

.PHONY: rust-man
rust-man:
	@makepkg -p rust-man.PKGBUILD

.PHONY: chromium-man
chromium-man:
	@makepkg -p chromium-man.PKGBUILD

.PHONY: all
all: awesome-noauto kitty-xsession rust-man chromium-man

